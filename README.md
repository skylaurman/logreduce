# Log Reduce #

### This is a python package that handles getting rid of re occuring lines. Any lines the program is not trained on (aka ./success-logs) will be considered anomolies and put into an HTML file for easy digesting. The files you are training on MUST be similar or the package will not work. ###

## What is inside? ##

* Sumo.zip are files to train on and are considered 'Success logs'
* Sumo2.zip are files to test on and are considered 'Fail logs'
* logreduce.ipynb shows an example of the log reduce package in action

## Setup ##

* Dependencies will be installed in the notebook
* The output will be saved to test.html
* Be sure that the sumo and sumo2 files are placed in directories or modify the location


### Contact? ###

* [Email](skylaurman@gmail.com)
* [LinkedIn](https://www.linkedin.com/in/skylaanne/)